# ci4rc - Chemical Industry 4.0 Readiness Check

Multi-lingual (English / German / Italian) version of [Readiness-Check Chemie 4.0](https://www.vci.de/die-branche/chemie-40/services/readiness-check-tool-chemie-4-punkt-0-wie-gut-ist-ihr-unternehmen-auf-digitale-und-zirkulaere-transformation-vorbereitet.jsp) © 2019 VCI, Deloitte, BAVC; based on Version 2.0 - 01. November 2019

This survey is open source, code is [here](https://gitlab.com/simevo/ci4rc).

Survey implementation based on [surveyjs.io](https://surveyjs.io/).

Translations courtesy [www.DeepL.com/Translator (free version)](https://www.deepl.com/translator)

> ATTENTION Work-in-progress !
> Currently only the 1st sheet ("1. Strategie und Organisation") is implemented, and the back-end is not implemented.

ATM the survey is simply printed in JSON format:

```
 {
  "question1": {
    "q1": 2,
    "q2": 3,
    "q3": 2,
    "q6": 2,
    "q7": 3,
    "q9": 3,
    "q10": 2,
    "q11": 1
  },
  "question2": {
    "q17": 3,
    "q16": 3,
    "q14": 4,
    "q12": 3,
    "q13": 1
  },
  "question3": {
    "q18": 3,
    "q19": 4,
    "q20": 3,
    "q21": 2
  },
  "question4": {
    "q26": 5,
    "q27c": 5,
    "q25": 5,
    "q24": 5,
    "q23": 3,
    "q27a": 4,
    "q27b": 3
  }
 }
 ```

## Demo

![Animated GIF](/public/demo.gif).

## Project setup
```
yarnpkg install
```

### Compiles and hot-reloads for development
```
yarnpkg serve
```

### Compiles and minifies for production
```
yarnpkg build
```

### Lints and fixes files
```
yarnpkg lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## License

**ci4rc** - Chemical Industry 4.0 Readiness Check
Original Readiness-Check Chemie 4.0 (C) 2019 VCI, Deloitte, BAVC.
Web version Copyright (C) 2019-2020 simevo s.r.l.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program (file [LICENSE](/LICENSE)).
If not, see <https://www.gnu.org/licenses/>.
